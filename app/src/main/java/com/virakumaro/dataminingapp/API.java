package com.virakumaro.dataminingapp;

import android.content.Context;
import android.util.Log;

import com.android.volley.AuthFailureError;
import com.android.volley.Cache;
import com.android.volley.NetworkResponse;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.HttpHeaderParser;
import com.android.volley.toolbox.JsonObjectRequest;
import com.virakumaro.dataminingapp.Common.AppSingleton;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.UnsupportedEncodingException;
import java.util.HashMap;
import java.util.Map;

import static com.android.volley.VolleyLog.TAG;

public class API {

    private static String BASE_URL = "http://datamining.wilimsius.com/api/";

    public static String ALL_LOAN_CHECK = BASE_URL + "all_loan_check";
    public static String LOAN_DECISION = BASE_URL + "loan_decision";
    public static String REGION_COMPARE = BASE_URL + "region_compare";

    public static void baseRequest(final int method, final Context context, final String url, final JSONObject request, final ServerListener listener) {
        Log.e(TAG, "request url: " +  url + " : " + request.toString());
        JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(method, url, request, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                listener.onSuccess(response);
                Log.e(TAG,url + " : " + response.toString());

            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                listener.onFailed(error.networkResponse == null ? 400 : error.networkResponse.statusCode, error.getMessage());
                error.printStackTrace();
            }
        }){
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> headers = new HashMap<>();
                headers.put("Content-Type", "application/x-www-form-urlencoded");
                return headers;
            }

        };
        AppSingleton.getInstance(context).addToRequestQueue(jsonObjectRequest, API.class.getSimpleName());
    }

    public static void cacheRequest(final int method, final Context context, final String url, final ServerListener listener) {
        CacheRequest jsonObjectRequest = new CacheRequest(method, url, new Response.Listener<NetworkResponse>() {
            @Override
            public void onResponse(NetworkResponse response) {
                try {
                    final String jsonString = new String(response.data,
                            HttpHeaderParser.parseCharset(response.headers));
                    JSONObject jsonObject = new JSONObject(jsonString);
                    listener.onSuccess(jsonObject);
                } catch (UnsupportedEncodingException | JSONException e) {
                    e.printStackTrace();
                    listener.onFailed(400, e.getMessage());
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                listener.onFailed(error.networkResponse == null ? 400 : error.networkResponse.statusCode, error.getMessage());
            }
        });
        AppSingleton.getInstance(context).addToRequestQueue(jsonObjectRequest, API.class.getSimpleName());
    }


    private static class CacheRequest extends Request<NetworkResponse> {
        private final Response.Listener<NetworkResponse> mListener;
        private final Response.ErrorListener mErrorListener;

        public CacheRequest(int method, String url, Response.Listener<NetworkResponse> listener, Response.ErrorListener errorListener) {
            super(method, url, errorListener);
            this.mListener = listener;
            this.mErrorListener = errorListener;
        }


        @Override
        protected Response<NetworkResponse> parseNetworkResponse(NetworkResponse response) {
            Cache.Entry cacheEntry = HttpHeaderParser.parseCacheHeaders(response);
            if (cacheEntry == null) {
                cacheEntry = new Cache.Entry();
            }
            final long cacheHitButRefreshed = 10 * 1000; // in 10 secs cache will be hit, but also refreshed on background
            final long cacheExpired = 14 * 24 * 60 * 60 * 1000; // in 14 days this cache entry expires completely
            long now = System.currentTimeMillis();
            final long softExpire = now + cacheHitButRefreshed;
            final long ttl = now + cacheExpired;
            cacheEntry.data = response.data;
            cacheEntry.softTtl = softExpire;
            cacheEntry.ttl = ttl;
            String headerValue;
            headerValue = response.headers.get("Date");
            if (headerValue != null) {
                cacheEntry.serverDate = HttpHeaderParser.parseDateAsEpoch(headerValue);
            }
            headerValue = response.headers.get("Last-Modified");
            if (headerValue != null) {
                cacheEntry.lastModified = HttpHeaderParser.parseDateAsEpoch(headerValue);
            }
            cacheEntry.responseHeaders = response.headers;
            return Response.success(response, cacheEntry);
        }

        @Override
        protected void deliverResponse(NetworkResponse response) {
            mListener.onResponse(response);
        }

        @Override
        protected VolleyError parseNetworkError(VolleyError volleyError) {
            return super.parseNetworkError(volleyError);
        }

        @Override
        public void deliverError(VolleyError error) {
            mErrorListener.onErrorResponse(error);
        }
    }


    public interface ServerListener {
        void onSuccess(JSONObject response);
        void onFailed(int errorCode, String message);
    }


}
