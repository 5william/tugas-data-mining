package com.virakumaro.dataminingapp.Activity;

import android.graphics.Color;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Spinner;

import com.android.volley.Request;
import com.github.mikephil.charting.animation.Easing;
import com.github.mikephil.charting.charts.RadarChart;
import com.github.mikephil.charting.components.AxisBase;
import com.github.mikephil.charting.components.Legend;
import com.github.mikephil.charting.components.MarkerView;
import com.github.mikephil.charting.components.XAxis;
import com.github.mikephil.charting.components.YAxis;
import com.github.mikephil.charting.data.RadarData;
import com.github.mikephil.charting.data.RadarDataSet;
import com.github.mikephil.charting.data.RadarEntry;
import com.github.mikephil.charting.formatter.IAxisValueFormatter;
import com.github.mikephil.charting.interfaces.datasets.IRadarDataSet;
import com.virakumaro.dataminingapp.API;
import com.virakumaro.dataminingapp.Common.JSONParserHelper;
import com.virakumaro.dataminingapp.Model.MonthDataModel;
import com.virakumaro.dataminingapp.Model.RegionDataModel;
import com.virakumaro.dataminingapp.R;
import com.virakumaro.dataminingapp.Common.RadarMarkerView;

import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

public class DataLoanPerMonthActivity extends AppCompatActivity {
    private RadarChart chart;
    private List<MonthDataModel> monthDataModels = new ArrayList<>();
    private int currentA = 0;
    private int currentB = 0;
    private Spinner spinnerA, spinnerB;
    private boolean loadDone = false;
    private ArrayAdapter<String> adapter;
    private final String[] mRegion = new String[]{"Jakarta Timur", "Jakarta Utara", "Jakarta Pusat", "Jakarta Barat", "Jakarta Selatan"};
    private final String[] mMonth = new String[]{"January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December"};

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_data_loan_per_month);
        chart = findViewById(R.id.chart);
        spinnerA = findViewById(R.id.spinner1);
        spinnerB = findViewById(R.id.spinner2);

        chart.setBackgroundColor(Color.rgb(60, 65, 82));

        chart.getDescription().setEnabled(false);

        chart.setWebLineWidth(1f);
        chart.setWebColor(Color.LTGRAY);
        chart.setWebLineWidthInner(1f);
        chart.setWebColorInner(Color.LTGRAY);
        chart.setWebAlpha(100);

        // create a custom MarkerView (extend MarkerView) and specify the layout
        // to use for it
        MarkerView mv = new RadarMarkerView(this, R.layout.radar_markerview);
        mv.setChartView(chart); // For bounds control
        chart.setMarker(mv); // Set the marker to the chart

        XAxis xAxis = chart.getXAxis();
        //xAxis.setTypeface(tfLight);
        xAxis.setTextSize(9f);
        xAxis.setYOffset(0f);
        xAxis.setXOffset(0f);
        xAxis.setValueFormatter(new IAxisValueFormatter() {
            @Override
            public String getFormattedValue(float value, AxisBase axis) {
                return mRegion[(int) value % mRegion.length];
            }
        });
        xAxis.setTextColor(Color.WHITE);

        YAxis yAxis = chart.getYAxis();
        //yAxis.setTypeface(tfLight);
        //yAxis.setLabelCount(5, false);
        yAxis.setTextSize(9f);
        yAxis.setAxisMinimum(0f);
        //yAxis.setAxisMaximum(80f);
        yAxis.setDrawLabels(false);

        Legend l = chart.getLegend();
        l.setVerticalAlignment(Legend.LegendVerticalAlignment.TOP);
        l.setHorizontalAlignment(Legend.LegendHorizontalAlignment.CENTER);
        l.setOrientation(Legend.LegendOrientation.HORIZONTAL);
        l.setDrawInside(false);
        //l.setTypeface(tfLight);
        l.setXEntrySpace(7f);
        l.setYEntrySpace(5f);
        l.setTextColor(Color.WHITE);



        API.cacheRequest(Request.Method.GET, this, API.REGION_COMPARE, new API.ServerListener() {
            @Override
            public void onSuccess(JSONObject response) {
                monthDataModels = JSONParserHelper.getAllDataMonths(response);
                if (!loadDone) {
                    setData(currentA,currentB);
                    loadDone = true;
                }
            }

            @Override
            public void onFailed(int errorCode, String message) {

            }
        });
        adapter = new ArrayAdapter<>(this, android.R.layout.simple_spinner_dropdown_item, mMonth);
        spinnerA.setAdapter(adapter);
        spinnerB.setAdapter(adapter);

        spinnerA.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                setData(position,currentB);
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
        spinnerB.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                setData(currentA, position);
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });


    }

    public void setData(int a, int b) {
        if(monthDataModels.isEmpty())
            return;
        currentA = a;
        currentB = b;
        List<RegionDataModel> dataModelA = monthDataModels.get(a).getRegionList();
        List<RegionDataModel> dataModelB = monthDataModels.get(b).getRegionList();

        ArrayList<RadarEntry> entries1 = new ArrayList<>();
        ArrayList<RadarEntry> entries2 = new ArrayList<>();

        for (int i = 0; i < 5; i++) {
            entries1.add(new RadarEntry(dataModelA.get(i).getTotalLoanAmount()));
            entries2.add(new RadarEntry(dataModelB.get(i).getTotalLoanAmount()));
        }

        RadarDataSet set1 = new RadarDataSet(entries1, mMonth[a]);
        set1.setColor(Color.rgb(103, 110, 129));
        set1.setFillColor(Color.rgb(103, 110, 129));
        set1.setDrawFilled(true);
        set1.setFillAlpha(180);
        set1.setLineWidth(2f);
        set1.setDrawHighlightCircleEnabled(true);
        set1.setDrawHighlightIndicators(false);

        RadarDataSet set2 = new RadarDataSet(entries2, mMonth[b]);
        set2.setColor(Color.rgb(121, 162, 175));
        set2.setFillColor(Color.rgb(121, 162, 175));
        set2.setDrawFilled(true);
        set2.setFillAlpha(180);
        set2.setLineWidth(2f);
        set2.setDrawHighlightCircleEnabled(true);
        set2.setDrawHighlightIndicators(false);

        ArrayList<IRadarDataSet> sets = new ArrayList<>();
        sets.add(set1);
        sets.add(set2);

        RadarData data = new RadarData(sets);
        //data.setValueTypeface(tfLight);
        data.setValueTextSize(8f);
        data.setDrawValues(false);
        data.setValueTextColor(Color.WHITE);

        chart.setData(data);
        chart.invalidate();
        chart.animateX(1400, Easing.EasingOption.EaseInOutQuad);



    }

}
