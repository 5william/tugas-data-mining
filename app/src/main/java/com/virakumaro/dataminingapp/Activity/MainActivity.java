package com.virakumaro.dataminingapp.Activity;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

import com.virakumaro.dataminingapp.R;

public class MainActivity extends AppCompatActivity {
    private Button btnPinjamUang, btnDecide, btnPerMonth;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        btnPinjamUang = findViewById(R.id.btn_pinjam_uang);
        btnDecide = findViewById(R.id.btn_decide_loan);
        btnPerMonth = findViewById(R.id.btn_loan_per_month);
        btnPinjamUang.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(MainActivity.this, PinjamUangActivity.class));
            }
        });
        btnDecide.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(MainActivity.this, LoanDecideActivity.class));
            }
        });
        btnPerMonth.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(MainActivity.this, DataLoanPerMonthActivity.class));
            }
        });
    }
}
