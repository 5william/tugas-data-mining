package com.virakumaro.dataminingapp.Activity;

import android.graphics.Color;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Spinner;

import com.android.volley.Request;
import com.github.mikephil.charting.charts.PieChart;
import com.github.mikephil.charting.data.PieData;
import com.github.mikephil.charting.data.PieDataSet;
import com.github.mikephil.charting.data.PieEntry;
import com.github.mikephil.charting.utils.ColorTemplate;
import com.virakumaro.dataminingapp.API;
import com.virakumaro.dataminingapp.Common.JSONParserHelper;
import com.virakumaro.dataminingapp.Model.LoanModel;
import com.virakumaro.dataminingapp.Model.LoanPurposeDetailModel;
import com.virakumaro.dataminingapp.R;

import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

public class PinjamUangActivity extends AppCompatActivity {
    private PieChart pieChart;
    private List<LoanModel> loanModels;
    private ArrayAdapter<String> adapter;
    private Spinner spinner;
    private boolean loadDone=false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_pinjam_uang);

        spinner = findViewById(R.id.spinner);
        pieChart = findViewById(R.id.chart);


        API.cacheRequest(Request.Method.GET, this, API.ALL_LOAN_CHECK, new API.ServerListener() {
            @Override
            public void onSuccess(JSONObject response) {
                loanModels = JSONParserHelper.getAllLoan(response);
                String[] judul = new String[loanModels.size()];
                for (int i = 0; i < loanModels.size(); i++) {
                    judul[i] = loanModels.get(i).getName();
                }
                if (adapter == null) {
                    adapter = new ArrayAdapter<>(PinjamUangActivity.this, android.R.layout.simple_spinner_dropdown_item, judul);
                    spinner.setAdapter(adapter);
                }

                ArrayList<PieEntry> yVals = new ArrayList<>();
                List<LoanPurposeDetailModel> detailModels = loanModels.get(0).getDetailModels();
                for (int i = 0; i < detailModels.size(); i++) {
                    if(detailModels.get(i).getTotal()>1)
                        yVals.add(new PieEntry(detailModels.get(i).getTotal(), detailModels.get(i).getPurpose()));
                }
                if (!loadDone) {
                    setupPieChart(yVals);
                    loadDone = true;
                }

            }

            @Override
            public void onFailed(int errorCode, String message) {
                //Toast.makeText(PinjamUangActivity.this, "Failed", Toast.LENGTH_SHORT).show();
            }
        });

        spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                ArrayList<PieEntry> yVals = new ArrayList<>();
                List<LoanPurposeDetailModel> detailModels = loanModels.get(position).getDetailModels();
                for (int i = 0; i < detailModels.size(); i++) {
                    if(detailModels.get(i).getTotal()>1)
                        yVals.add(new PieEntry(detailModels.get(i).getTotal(), detailModels.get(i).getPurpose()));
                }
                setupPieChart(yVals);
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });


    }

    private void setupPieChart(ArrayList<PieEntry> yVals) {
        pieChart.setUsePercentValues(true);
        pieChart.getDescription().setEnabled(false);
        pieChart.setExtraOffsets(5,10,5,5);
        pieChart.setDragDecelerationFrictionCoef(0.95f);
        pieChart.setDrawHoleEnabled(true);
        pieChart.setHoleColor(Color.WHITE);
        pieChart.setTransparentCircleRadius(61f);
        pieChart.animateX(1400);

        PieDataSet dataSet = new PieDataSet(yVals, "Data Peminjaman Uang");
        dataSet.setSliceSpace(3f);
        dataSet.setSelectionShift(5f);
        ArrayList<Integer> colors = new ArrayList<>();

        for (int c : ColorTemplate.VORDIPLOM_COLORS)
            colors.add(c);

        for (int c : ColorTemplate.JOYFUL_COLORS)
            colors.add(c);

        for (int c : ColorTemplate.COLORFUL_COLORS)
            colors.add(c);

        for (int c : ColorTemplate.LIBERTY_COLORS)
            colors.add(c);

        for (int c : ColorTemplate.PASTEL_COLORS)
            colors.add(c);

        dataSet.setColors(colors);

        PieData data = new PieData(dataSet);

        pieChart.setEntryLabelColor(Color.BLACK);
        pieChart.setData(data);
        pieChart.invalidate();
    }
}
