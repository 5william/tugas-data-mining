package com.virakumaro.dataminingapp.Activity;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.virakumaro.dataminingapp.API;
import com.virakumaro.dataminingapp.Common.DialogLoading;
import com.virakumaro.dataminingapp.R;

import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class LoanDecideActivity extends AppCompatActivity {
    private EditText etLoanAmount, etAnnualIncome;
    private Spinner spRegion, spPurpose, spHome;
    private TextView tvResult;
    private ArrayAdapter<String> adRegion, adPurpose, adHome;
    private Button btnSubmit;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_loan_decide);
        setTitle("Decide Loan");
        etLoanAmount = findViewById(R.id.loan_amount);
        tvResult = findViewById(R.id.text_result);
        etAnnualIncome = findViewById(R.id.annual_income);
        spRegion = findViewById(R.id.spinner_region);
        spPurpose = findViewById(R.id.spinner_purpose);
        spHome = findViewById(R.id.spinner_home_ownership);
        btnSubmit = findViewById(R.id.btn_submit);
        declareAdapter();

        btnSubmit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                final DialogLoading dialogLoading =
                        DialogLoading.newInstance("Analyzing Result...", false);
                dialogLoading.show(getSupportFragmentManager(), "");
                Map<String, Object> params = new HashMap<>();
                params.put("region_id",spRegion.getSelectedItemPosition() + 1);
                params.put("purpose_cat", spPurpose.getSelectedItemPosition() + 1);
                params.put("loan_amount", etLoanAmount.getText().toString());
                params.put("home_ownership_cat", spHome.getSelectedItemPosition() + 1);
                params.put("annual_income", etAnnualIncome.getText().toString());
                API.baseRequest(Request.Method.POST, LoanDecideActivity.this, API.LOAN_DECISION, new JSONObject(params), new API.ServerListener() {
                    @Override
                    public void onSuccess(JSONObject response) {
                        dialogLoading.dismiss();
                        tvResult.setText(response.optString("message"));
                    }

                    @Override
                    public void onFailed(int errorCode, String message) {
                        dialogLoading.dismiss();
                        Toast.makeText(LoanDecideActivity.this, "Network error", Toast.LENGTH_SHORT).show();
                    }
                });
            }
        });

    }


    private void declareAdapter() {
        List<String> stringRegion = new ArrayList<>();
        stringRegion.add("Jakarta Timur");
        stringRegion.add("Jakarta Utara");
        stringRegion.add("Jakarta Pusat");
        stringRegion.add("Jakarta Barat");
        stringRegion.add("Jakarta Selatan");
        adRegion = new ArrayAdapter<>(this, android.R.layout.simple_spinner_dropdown_item, stringRegion);
        spRegion.setAdapter(adRegion);

        List<String> stringPurpose = new ArrayList<>();
        stringPurpose.add("Credit Card");
        stringPurpose.add("Car");
        stringPurpose.add("Small Business");
        stringPurpose.add("FnB");
        stringPurpose.add("Wedding");
        stringPurpose.add("Debt Consolidation");
        stringPurpose.add("Home Improvement");
        stringPurpose.add("Major Purchase");
        stringPurpose.add("Medical");
        stringPurpose.add("Moving");
        stringPurpose.add("Vacation");
        stringPurpose.add("House");
        stringPurpose.add("Renewable Energy");
        adPurpose = new ArrayAdapter<>(this, android.R.layout.simple_spinner_dropdown_item, stringPurpose);
        spPurpose.setAdapter(adPurpose);

        List<String> stringHome = new ArrayList<>();
        stringHome.add("Rent");
        stringHome.add("Own");
        stringHome.add("Mortgage");
        adHome = new ArrayAdapter<>(this, android.R.layout.simple_spinner_dropdown_item, stringHome);
        spHome.setAdapter(adHome);
    }
}
