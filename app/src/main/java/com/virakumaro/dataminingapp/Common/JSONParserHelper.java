package com.virakumaro.dataminingapp.Common;

import com.virakumaro.dataminingapp.Model.LoanModel;
import com.virakumaro.dataminingapp.Model.LoanPurposeDetailModel;
import com.virakumaro.dataminingapp.Model.MonthDataModel;
import com.virakumaro.dataminingapp.Model.RegionDataModel;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

public class JSONParserHelper {
    public static List<LoanModel> getAllLoan(JSONObject response) {
        List<LoanModel> listLoan = new ArrayList<>();
        JSONArray jsonArray = response.optJSONArray("data");
        for (int i = 0; i < jsonArray.length(); i++) {
            LoanModel loanModel = new LoanModel(jsonArray.optJSONObject(i));
            loanModel.setDetailModels(getAllDetails(jsonArray.optJSONObject(i)));
            listLoan.add(loanModel);
        }
        return listLoan;
    }

    private static List<LoanPurposeDetailModel> getAllDetails(JSONObject response) {
        List<LoanPurposeDetailModel> listDetail = new ArrayList<>();
        JSONArray jsonArray = response.optJSONArray("purpose_detail");
        for (int i=0; i < jsonArray.length(); i++) {
            listDetail.add(new LoanPurposeDetailModel(jsonArray.optJSONObject(i)));
        }

        return listDetail;
    }
    public static List<MonthDataModel> getAllDataMonths(JSONObject response) {
        List<MonthDataModel> listMonths = new ArrayList<>();
        JSONArray jsonArray = response.optJSONArray("data");
        for (int i = 0; i < jsonArray.length(); i++) {
            MonthDataModel monthModel = new MonthDataModel(jsonArray.optJSONObject(i));
            monthModel.setRegionList(getAllRegions(jsonArray.optJSONObject(i)));
            listMonths.add(monthModel);
        }
        return listMonths;
    }

    private static List<RegionDataModel> getAllRegions(JSONObject response) {
        List<RegionDataModel> listRegion = new ArrayList<>();
        JSONArray jsonArray = response.optJSONArray("data");
        for (int i = 0; i < jsonArray.length(); i++) {
            listRegion.add(new RegionDataModel(jsonArray.optJSONObject(i)));
        }
        return listRegion;
    }
}
