package com.virakumaro.dataminingapp.Common;

import android.app.Dialog;
import android.app.ProgressDialog;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.DialogFragment;

import com.virakumaro.dataminingapp.R;


/**
 * Created by Ajisantoso on 23/01/2017.
 */

public class DialogLoading extends DialogFragment {
    private String title = "";
    private boolean isCancelable = false;

    public static DialogLoading newInstance() {
        DialogLoading dialogLoading = new DialogLoading();
        dialogLoading.title = "Default Loading...";
        return dialogLoading;
    }

    public static DialogLoading newInstance(String title) {
        DialogLoading dialogLoading = new DialogLoading();
        dialogLoading.title = title;
        return dialogLoading;
    }

    public static DialogLoading newInstance(String title, boolean isCancelable) {
        DialogLoading dialogLoading = new DialogLoading();
        dialogLoading.isCancelable = isCancelable;
        dialogLoading.title = title;
        return dialogLoading;
    }

    @NonNull
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        ProgressDialog progressDialog = new ProgressDialog(getContext(), R.style.DialogTheme);
        progressDialog.setMessage(title);

        setCancelable(isCancelable);
        return progressDialog;
    }
}

