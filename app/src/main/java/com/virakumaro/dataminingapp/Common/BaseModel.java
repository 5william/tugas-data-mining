package com.virakumaro.dataminingapp.Common;

import android.os.Parcel;
import org.json.JSONException;
import org.json.JSONObject;

public class BaseModel {
    public static final String _ID = "id";
    public static final String _ID_UPPER = "ID";
    private long id;

    public long getId() {
        return this.id;
    }

    public int describeContents() {
        return 0;
    }

    public BaseModel setId(long id) {
        this.id = id;
        return this;
    }

    public BaseModel() {
    }

    protected BaseModel(long id) {
        this.id = id;
    }

    protected BaseModel(Parcel in) {
        this.id = in.readLong();
    }

    protected BaseModel(JSONObject json) {
        this.id = json == null ? 0L : (json.isNull("id") ? json.optLong("ID") : json.optLong("id"));
    }

    public void writeToParcel(Parcel dest, int flags) {
        dest.writeLong(this.id);
    }

    protected String optString(JSONObject json, String key) {
        return json.isNull(key) ? null : json.optString(key);
    }

    public JSONObject toJsonObject() throws JSONException {
        JSONObject jsonObject = new JSONObject();
        jsonObject.put("id", this.id);
        return jsonObject;
    }
}


